''' @file Keypad.py

@brief      Keypad module.
@details    Allows for a 4x4 keypad to be used, by scanning the rows and columns. \n
            https://bitbucket.org/jrraikin/final-project/src/master/Keypad.py
'''

import pyb
import utime

def main():
    '''Initialuzes the rows and colums to be used in the keypad module.
    '''
    #pins
    r1 = pyb.Pin(pyb.Pin.cpu.C8, pyb.Pin.IN)
    r2 = pyb.Pin(pyb.Pin.cpu.B13, pyb.Pin.IN)
    r3 = pyb.Pin(pyb.Pin.cpu.C5, pyb.Pin.IN)
    r4 = pyb.Pin(pyb.Pin.cpu.A9, pyb.Pin.IN)
    c4 = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)
    c3 = pyb.Pin(pyb.Pin.cpu.B11, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)
    c2 = pyb.Pin(pyb.Pin.cpu.B12, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)
    c1 = pyb.Pin(pyb.Pin.cpu.A8, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)

    rows = [r1, r2, r3, r4]
    cols = [c1, c2, c3, c4]

    keys = Keypad(rows, cols, '')

    word = ''
    while (len(word) < 5):
        word += keys.get_key()
        print(word)
    '''keys.get_key()
    print(keys.char)
    keys.get_key()
    print(keys.char)'''

class Keypad:
    '''Gets keys pressed from the keypad.
    '''
    def __init__(self, row_arr =  [pyb.Pin(pyb.Pin.cpu.C8, pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.B13, pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.C5, pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.A9, pyb.Pin.IN)], col_arr = [pyb.Pin(pyb.Pin.cpu.A8, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), pyb.Pin(pyb.Pin.cpu.B12, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), pyb.Pin(pyb.Pin.cpu.B11, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)], pin_mode = 'default'):
        '''Initializes keypad. Sets up relationship between pins with rows and columns, sets up keys, and sets base char as ''
            If rows and columns paramters not given, default pins provided.
            @param row_arr   Array of row pins, initialized as inputs. \n
                             default should be: \n
                             [pyb.Pin(pyb.Pin.cpu.C8, pyb.Pin.IN), \n
                             pyb.Pin(pyb.Pin.cpu.B13, pyb.Pin.IN), \n
                             pyb.Pin(pyb.Pin.cpu.C5, pyb.Pin.IN), \n
                             pyb.Pin(pyb.Pin.cpu.A9, pyb.Pin.IN)]
            @param col_arr   Array of column pins, initilaized as inputs pulled down. \n
                             defualt should be: \n
                             [pyb.Pin(pyb.Pin.cpu.A8, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), \n
                             pyb.Pin(pyb.Pin.cpu.B12, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), \n
                             pyb.Pin(pyb.Pin.cpu.B11, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), \n
                             pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)])
        '''
        self._pin_mode = pin_mode
        if self._pin_mode == 'default':
            self._rows =  [pyb.Pin(pyb.Pin.cpu.C8, pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.C6, pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.C5, pyb.Pin.IN), pyb.Pin(pyb.Pin.cpu.A9, pyb.Pin.IN)]
            self._cols = [pyb.Pin(pyb.Pin.cpu.A8, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), pyb.Pin(pyb.Pin.cpu.B12, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), pyb.Pin(pyb.Pin.cpu.B11, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN), pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)]
        else:
            self._rows = row_arr
            self._cols = col_arr
        ##keys pressed related to this matrix of rows and columns, which match keypads.
        self.keys = [['1', '2', '3', 'A'],
                     ['4', '5', '6', 'B'],
                     ['7', '8', '9', 'C'],
                     ['*', '0', '#', 'D']]
        ##character is '' as its base, but if pressed will change to value assocciated on matrix.
        self.char = ''

    def key_scan(self):
        '''Gets key pressed. Scans through rows and columns, finding when column value is high.
            @return Pressed key or ''.
        '''
        #self.__init__(self._rows, self._cols, self._pin_mode)
        i = 0
        for row in self._rows:
            row.init(pyb.Pin.OUT_PP, value = 1)
            temp = 0
            j = 0
            for col in self._cols:
                if (col.value() and row.value()):
                    #print('pressed:', self.keys[i][j])
                    self.char = self.keys[i][j]
                utime.sleep_us(50)
                j += 1
            row.init(pyb.Pin.IN)
            i += 1
        return self.char

    def get_key(self):
        '''Gets key from scanned keys.
            @return Pressed key.
        '''
        #self.__init__(self._rows, self._cols, self._pin_mode)
        self.char = ''
        while(self.char == ''):
            self.key_scan()
            #print(keys.char)
            utime.sleep_ms(50)
        utime.sleep_ms(100)
        return self.char

if __name__ == '__main__':
    main()
