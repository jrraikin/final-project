'''
@file       main.py

@brief Link to Code: https://bitbucket.org/jrraikin/final-project/src/master/main.py
'''
import pyb
import utime

from Keypad import *
from IMU import *
from Motor import *
from Location import *

def main():
    '''Runs project module in a while loop that breaks if mode input is not one of the options.
    Options:
    - Duty cycle
    - Distance
    - Degree
    '''
    p = Project()
    #p.init_parts()
    #keypad_mode()
    sw = pyb.Switch()
    x = True
    while(x):
        x = p.keypad_mode()
        #sw.callback(p.keypad_mode()) #switch interrupt to change mode
    #p.sw.callback(p.keypad_mode()) #switch interrupt to change mode

class Project:
    '''This class allows for the project to run with the other modules simultaneously.
    '''
    def __init__(self):
        '''Initializes all other classes in order to run project.
        Classes include:
        - Keypad
        - Encoder
        - IMU
        - Motor
        '''
        #self.init_parts()
        ##Switch object (interrupt)
        self.sw = pyb.Switch()
        ##Keypad object
        r1 = pyb.Pin(pyb.Pin.cpu.C8, pyb.Pin.IN)
        r2 = pyb.Pin(pyb.Pin.cpu.B13, pyb.Pin.IN)
        r3 = pyb.Pin(pyb.Pin.cpu.C5, pyb.Pin.IN)
        r4 = pyb.Pin(pyb.Pin.cpu.A9, pyb.Pin.IN)
        c4 = pyb.Pin(pyb.Pin.cpu.B2, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)
        c3 = pyb.Pin(pyb.Pin.cpu.B11, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)
        c2 = pyb.Pin(pyb.Pin.cpu.B12, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)
        c1 = pyb.Pin(pyb.Pin.cpu.A8, pyb.Pin.IN, pull = pyb.Pin.PULL_DOWN)

        rows = [r1, r2, r3, r4]
        cols = [c1, c2, c3, c4]

        self.keys = Keypad(rows, cols, '')

        pin_ENA = pyb.Pin.cpu.A10
        pin_INA1 = pyb.Pin.cpu.B4
        pin_INA2 = pyb.Pin.cpu.B5
        timAM = 3
        timAE = 4
        pin_encAA = pyb.Pin.cpu.B6
        pin_encAB = pyb.Pin.cpu.B7
        ##Motor A Object
        self.mA = MotorDriver(pin_ENA, pin_INA1, pin_INA2, timAM)
        self.mA.disable()
        ##Encoder A Object
        self.encA = Encoder(pin_encAA, pin_encAB, timAE)

        pin_ENB = pyb.Pin.cpu.C1
        pin_INB1 = pyb.Pin.cpu.A0
        pin_INB2 = pyb.Pin.cpu.A1
        timBM = 5
        timBE = 8
        pin_encBA = pyb.Pin.cpu.C7
        pin_encBB = pyb.Pin.cpu.C6
        ##Motor B Object
        self.mB = MotorDriver(pin_ENB, pin_INB1, pin_INB2, timBM)
        self.mB.disable()
        ##Encoder B Object
        self.encB = Encoder(pin_encBA, pin_encBB, timBE)

    def keypad_mode(self):
        '''Input mode of keypad to control motor.
           Options between Duty Cycle (1), Distance (2), and Degrees (3)
                @return True if valid mode entered, False otherwise.
        '''
        mode = input('\nWith keyboard, enter Motor Mode: ')
        #print('\nWith keyboard, enter Motor Mode: ')
        #print('\t[Mode Options: Duty Cycle (1), Distance (2), and Degrees (3)]')
        #mode = input('>>> ')
        if mode == '1' or mode == 'DC' or mode == 'dc' or mode == 'duty cycle' or mode == 'Duty Cycle':
            print('\tMode: Duty Cycle')
            self.keypad_to_motor_DC()
            return True
        elif mode == '2' or mode == 'distance' or mode == 'dist' or mode == 'Distance':
            print('\tMode: Distance')
            self.keypad_to_motor_distance()
            return True
        elif mode == '3' or mode == 'degrees' or mode == 'deg' or mode == 'DG' or mode == 'dg':
            print('\tMode: Degrees')
            self.keypad_to_motor_degree()
            return True
        else:
            print('\nThanks for playing!')
            return False


    def keypad_to_motor_DC(self):
        '''Uses duty cycle and time to control motor.'''
        dc = self.get_DC()
        if dc == False:
            print('\tNothing entered')
        else:
            t = self.get_time()
            if t != False:
                self.mA.enable()
                self.mA.set_duty(dc)
                for i in range(t):
                    utime.sleep(1)
                    if i == 0:
                        print('\ttime passed:', 1, 'second')
                    else:
                        print('\ttime passed:', i + 1, 'seconds')
                self.mA.disable()
            else:
                print('\tNothing entered')


    def get_DC(self):
        '''Keypad used to get duty cycle for motors.
        @return duty cycle if number inputted, False if only the '#' is entered.
        '''
        nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        dc = '0'
        print('\nWith keypad, enter duty cycle:')
        print("\t'#': enter \t'C': clear input \t'D': backspace \t'*': negative")
        char = self.keys.get_key()
        while char != '#':
            if char == '*' and dc == '0':
                dc = '-'
                print('\t>>>', dc)
            elif char in nums:
                dc += char
                if dc[0] == '0':
                    print('\t>>>', dc[1:])
                else:
                    print('\t>>>', dc)
            elif char == 'D':
                if dc == '0':
                    print('\t>>>', dc[1:])
                elif dc != '-':
                    dc = dc[:-1]
                    if dc[0] == '0':
                        print('\t>>>', dc[1:])
                    else:
                        print('\t>>>', dc)
                else:
                    dc = '0'
                    print('\t>>>', dc[1:])
            elif char == 'C':
                dc = '0'
                print('\tCleared')
                print('\t>>>')
            char = ''
            char = self.keys.get_key()
            utime.sleep_ms(100)
        if (dc == '0' and char == '#'):
            #print('\tNothing entered')
            return False
        else:
            dc = int(dc)
            print('Duty Cycle:', dc)
            return dc

    def get_dist(self):
        nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        dist = '0'
        print('\nWith keypad, enter desired distance:')
        print("\t'#': enter \t'C': clear input \t'D': backspace \t'*': negative")
        char = self.keys.get_key()
        while char != '#':
            if char == '*' and dist == '0':
                dist = '-'
                print('\t>>>', dist)
            elif char in nums:
                dist += char
                if dist[0] == '0':
                    print('\t>>>', dist[1:])
                else:
                    print('\t>>>', dist)
            elif char == 'D':
                if dist == '0':
                    print('\t>>>', dist[1:])
                elif dist != '-':
                    dist = dist[:-1]
                    if dist[0] == '0':
                        print('\t>>>', dist[1:])
                    else:
                        print('\t>>>', dist)
                else:
                    deg = '0'
                    print('\t>>>', deg[1:])
            elif char == 'C':
                dist = '0'
                print('\tCleared')
                print('\t>>>')
            char = ''
            char = self.keys.get_key()
            utime.sleep_ms(100)
        if (dist == '0' and char == '#'):
            #print('\tNothing entered')
            return False
        else:
            dist = int(dist)
            print('Distance:', dist)
            return dist


    def keypad_to_motor_distance(self):
        '''Keypad used to get distance for motor to travel.'''
        dist = self.get_dist()
        if dist == False:
            print('\tNothing entered')
        else:
            if dist < 0:
                dc = -14
            else:
                dc = 14
            self.encA.update()
            og_pos = self.encA.get_position()
            self.mA.enable()
            self.mA.set_duty(dc)
            curr_pos = int((self.encA.get_position() - og_pos)/100)
            while (abs(curr_pos) < abs(dist)):
                if abs(curr_pos) % 10 == 0:
                    if dist < 0:
                        print('\tCurrent position: -{0}'.format(abs(curr_pos)))
                    else:
                        print('\tCurrent position:', abs(curr_pos))
                self.encA.update()
                curr_pos = int((self.encA.get_position() - og_pos)/100)
            if dist < 0:
                print('\tCurrent position: -{0}'.format(abs(curr_pos)))
            else:
                print('\tCurrent position:', abs(curr_pos))
            self.mA.disable()


    def keypad_to_motor_degree(self):
        '''If degree is valid (pos/neg int), imu initiated.
            If positive degree entered, negative duty cycle inititated on motor until difference of entered degree with toal is less than 1.
            If negative degree entered, positive duty cycle inititated on motor until sum of entered degree with total is less than 1.
            Motors disabled after correct degree attained.
        '''
        ##IMU object.
        self.imu = IMU(1)
        deg = self.get_degrees()
        if deg == False:
            print('\tNothing entered')
        else:
            if deg >= 0:
                dc = -14
                self.mA.enable()
                self.mB.enable()
                self.encA.update()
                og_pos = self.encA.get_position()
                curr_pos = int((self.encA.get_position() - og_pos)/100)
                og_x = self.imu.euler_angles()[0]
                self.mA.set_duty(dc)
                self.mB.set_duty(dc)
                tot_deg = 0
                old_deg = self.imu.euler_angles()[0] - og_x
                new_deg = self.imu.euler_angles()[0] - og_x
                while (abs(tot_deg - deg) > 1):
                    self.encA.update()
                    curr_pos = int((self.encA.get_position() - og_pos)/100)
                    new_deg = self.imu.euler_angles()[0] - og_x
                    if new_deg < 10 and old_deg > 300:
                        tot_deg += new_deg + (360 - old_deg)
                    else:
                        tot_deg += (new_deg - old_deg)
                    old_deg = new_deg
                    print('\tCurrent degree:', tot_deg, '\tDistance travelled:', curr_pos)
                print('\tCurrent degree:', tot_deg, '\tDistance travelled:', curr_pos)
                self.mA.disable()
                self.mB.disable()
            else:
                dc = 14
                deg *= -1
                self.mA.enable()
                self.mB.enable()
                self.encB.update()
                og_pos = self.encB.get_position()
                curr_pos = int((self.encB.get_position() - og_pos)/100)
                og_x = self.imu.euler_angles()[0]
                self.mA.set_duty(dc)
                self.mB.set_duty(dc)
                tot_deg = 0
                old_deg = self.imu.euler_angles()[0] - og_x
                new_deg = self.imu.euler_angles()[0] - og_x
                while (abs(tot_deg + deg) > 1):
                    self.encB.update()
                    curr_pos = int((self.encB.get_position() - og_pos)/100)
                    new_deg = self.imu.euler_angles()[0] - og_x
                    if old_deg < 10 and new_deg > 300:
                        tot_deg += old_deg + (360 - new_deg)
                    else:
                        tot_deg += (new_deg - old_deg)
                    old_deg = new_deg
                    print('\tCurrent degree: {0}'.format(tot_deg), '\tDistance travelled:', curr_pos)
                print('\tCurrent degree: {0}'.format(tot_deg), '\tDistance travelled:', curr_pos)
                self.mA.disable()
                self.mB.disable()


    def get_degrees(self):
        '''Keypad inpiut for degrees.
        @return degrees (euler head).
        '''
        print('\nWith keypad, enter desired degrees:')
        print("\t'#': enter \t'C': clear input \t'D': backspace \t'*': negative")
        nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        deg = '0'
        char = self.keys.get_key()
        '''Get distance'''
        while char != '#':
            if char == '*' and deg == '0':
                deg = '-'
                print('\t>>>', deg)
            elif char in nums:
                deg += char
                if deg[0] == '0':
                    print('\t>>>', deg[1:])
                else:
                    print('\t>>>', deg)
            elif char == 'D':
                if deg == '0':
                    print('\t>>>', deg[1:])
                elif deg != '-':
                    deg = deg[:-1]
                    if deg[0] == '0':
                        print('\t>>>', deg[1:])
                    else:
                        print('\t>>>', deg)
                else:
                    deg = '0'
                    print('\t>>>', deg[1:])
            elif char == 'C':
                deg = '0'
                print('\tCleared')
                print('\t>>>')
            char = ''
            char = self.keys.get_key()
            utime.sleep_ms(100)
        if (deg == '0' and char == '#'):
            #print('\tNothing entered')
            return False
        else:
            deg = int(deg)
            print('Degree: {0}°'.format(deg))
            return deg



    def get_time(self):
        '''Keypad input for time.
        @return time (ms)
        '''
        print('\nWith keypad, enter desired time(s):')
        print("\t'#': enter \t'C': clear input \t'D': backspace")
        char = self.keys.get_key()
        nums = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        t = ''
        while char != '#':
            if char in nums:
                t += char
                print('\t>>>', t)
            elif char == 'D':
                if t != '':
                    t = t[:-1]
                    print('\t>>>', t)
                else:
                    print('\t>>>')
            elif char == 'C':
                t = ''
                print('\tCleared')
                print('\t>>>')
            else:
                print('\tPositive integer inputs only...')
            char = self.keys.get_key()
            utime.sleep_ms(100)
        if (t == '0' and char == '#'):
            return False
        else:
            t = int(t)
            print('Time: {0}s'.format(t))
            return t


if __name__ == '__main__':
    main()
