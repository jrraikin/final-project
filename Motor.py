'''
@file Motor.py

@brief      Motor module.
@details    Enable function, disable function, and set duty function (clockwise and counter-clockwise) \n
            Link to code: https://bitbucket.org/jrraikin/final-project/src/master/Motor.py
'''
#Josh Raikin

import pyb

def main():
    '''Establishes enable, in1 and 2 pins, and timer for motor module to perform.
    '''
    #pin objects to be interfaced with the motor
    pin_EN = pyb.Pin.cpu.A10
    pin_IN1 = pyb.Pin.cpu.B4
    pin_IN2 = pyb.Pin.cpu.B5
    tim = 3
    '''
    #for use of IN1B & IN2B:
    pin_EN = pyb.Pin.cpu.C1
    pin_IN1 = pyb.Pin.cpu.A0
    pin_IN2 = pyb.Pin.cpu.A1
    tim = 5
    '''
    #motor object created for passing in pins and timer
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

    #enables motor driver
    moe.enable()

    #sets dut cycle to 10 percent
    moe.set_duty(10)

    ''' delay '''
    for i in range (500000): # ~5 seconds
        x = 0
    moe.disable()

class MotorDriver:
    '''This class implements a motor driver for the ME405 board.
    '''
    def __init__(self, EN_pin, IN1_pin, IN2_pin, timer):
        '''Creates a motor driver by initializing GPIO and turning the motor off for safety.
        @param EN_pin   A pyb.Pin object to use as the enable pin.
        @param IN1_pin  A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin  A pyb.Pin object to use as the input to half bridge 2.
        @param timer    A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        '''
        ## A pyb.Pin object to use as the enable pin.
        self.EN_pin = EN_pin

        ## A pyb.Pin object to use as the input to half bridge 1.
        self.IN1_pin = IN1_pin

        ## A pyb.Pin object to use as the input to half bridge 2.
        self.IN2_pin = IN2_pin

        ##A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        self.timer = timer

        ENA = pyb.Pin(self.EN_pin, pyb.Pin.OUT_PP)
        ENA.low()
        print('Creating a motor driver')

    def enable(self):
        ENA = pyb.Pin(self.EN_pin, pyb.Pin.OUT_PP)
        ENA.high()
        print('Enabling motor')

    def disable(self):
        ENA = pyb.Pin(self.EN_pin, pyb.Pin.OUT_PP)
        ENA.low()
        print('Disabling motor')

    def set_duty(self, duty):
        '''This method sets the duty cycle to be sent to the motor
            to the given level.
            Positive values cause effort in one direction (ccw),
            negative values in the oppositie direction (cw).
            @param duty A signed integer holding the duty cycle of th PWM signal sent to the motor.

        '''
        if (duty > 0):
            #set IN1A low
            IN1A = pyb.Pin(self.IN1_pin, pyb.Pin.OUT_PP)
            IN1A.low()
            #set IN2A to PWM
            tim3 = pyb.Timer(self.timer, freq = 2000)
            t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
            t3ch2.pulse_width_percent(duty)
        else:
            #set IN2A low
            IN2A = pyb.Pin(self.IN2_pin, pyb.Pin.OUT_PP)
            IN2A.low()
            #set IN1A to PWM
            tim3 = pyb.Timer(self.timer, freq = 2000)
            t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
            t3ch1.pulse_width_percent(abs(duty))


if __name__ == '__main__':
    main()
