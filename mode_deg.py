'''
@file mode_deg.py

@brief      How Degree Mode Works:
@details    This page demonstrates how degree mode works.

@page       page_degs Degrees

@section    sec_vid Video of Operation
@details    https://drive.google.com/file/d/1gw-LOyUhzcSSu54UXdZxCu1X4mAjIcxX/view?usp=sharing

@section    sec_soft_arch Software Architecture
@image      html mode_deg.jpg width=50%
'''
