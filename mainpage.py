##
# @mainpage
# @section      sec_main_intro Introduction
#               For my ME 405 Final Project, I designed a cat toy. \n
#               It uses a keypad to take user input for duty cycle, time, distance, and degrees, and communicates to the motors, encoders, and the imu.
#               Using these features, it moves around a laser pointer. \n
#               Code link: https://bitbucket.org/jrraikin/final-project/src/master/main.py
#
#
# @section      sec_behavior Modes of Operation
#               This project utilizes the following modes. More details can be found on the \ref page_mode page.
#
#               The following modes are implemented:
#               - @ref page_dc "Duty Cycle"
#               - @ref page_dist "Distance"
#               - @ref page_degs "Degrees"
#
#
# @section      sec_description Description of Use
#               With the three available modes, the print screen will prompt the user for a keyboard input to decide which mode to use (modes listed above).
#               When a mode is selected, the print screen will prompt for a keypad input for either duty cycle and time, distance desired, and degrees.
#               The keypad allows one to use negative numbers with the '*' (not allowed for time input), backspace with the 'D', clear all with 'C', and enter with '#'.
#               When entering, the print display will show the inputted integer and then run the desired mode with those inputs.
#               This will then move the laser as desired. \n
#               After completing set mode, the print screen will prompt the user for mode again. If a mode is entered (such as 'new line' or 'x') that is not one of the options, the
#               code will exit the while loop and quit the program.
#
#
# @section      sec_photos Photos
#               @image html A.jpg width=50%
#               @image html B.jpg width=50%
#               @image html C.jpg width=50%
#               @image html D.jpg width=50%
#
#
# @section      sec_materials Materials
#               - Nucleo 64 STM32L476RG
#               - AC/DC Wall Mount Adapter
#               - CBL USB A-Mini Connector
#               - Breadbaord
#               - Wires
#               - 4x4 Matrix Membrane Keypad (#27899)
#               - Gearmotor 310 RPM 6V Micro Metal (2)
#               - Motor Driver
#               - IMU BNO055
#               - Laser Pointer
#               - Cardboard
#               - tape
#
# @section      sec_schem System Schematic
#               @image html schem.jpg   width=50%
#
#
# @section      sec_disc Disclaimer
#               This project was done under COVID-19 circumstances. If it were done under normal conditions, the following alterations would have been made: \n
#               - improved container for electrical components
#               - more asthetically pleasing end product
#               - ultrasonic sensor to detect movement to run program
#               - organized wiring
#               - LCD display rather than print display
#
#
#
#
# @author       Josh Raikin
# @copyright    License Info
# @date         June 1, 2020
