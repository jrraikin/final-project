'''
@file mode_dc.py

@brief      How Duty Cycle Mode Works:
@details    This page demonstrates how duty cycle mode works.

@page       page_dc Duty Cycle

@section    sec_vid Video of Operation
@details    https://drive.google.com/file/d/15o3oqr34gGREICmLGuxnTLjwrasU4L7l/view?usp=sharing

@section    sec_soft_arch Software Architecture
@image      html mode_dc.jpg width=50%
'''
