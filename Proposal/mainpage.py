## @file mainpage.py
#  @author Josh Raikin
#
#  Detailed doc for mainpage.py
#
#  @mainpage
#  @author Josh Raikin
#
#
#  @section sec_problem Purpose
#  Allows one to control a motor, which is attached to a laser pointer, using the keypad and imu.
#
#
#  @section sec_requirements Requirements
#  Two actuators:
#  - motor 1: controls laser pointer that rests on it
#  - motor 2: controls imu that rests on it
#  - motor 1 & 2 will both move in the exact same way. There are two as one motor cannot support the weight and balance both items.
#  \n
#  \n Two sensors:
#  - imu: verifies that keypad inputs meet the encoder output
#  - encoder: used as extra verification for keypad inout and imu
#  \n
#  \n Extra:
#  - keypad: allows for user input for either duty cycle and coordinate, or just coordinate (TBD)
#  - *ultrasonic sensor: this will be used to determine if person is close, which will initialize everything
#  \n *if time permits
#
#
#  @section sec_items Equipment
#  - keypad
#  - Nucleo
#  - 2 motors
#  - imu
#  - *laser pointer
#  - (if time permits: ultrasonic sensor)
#  \n *need to purchase still
#
#
#  @section sec_plan Manufacturing/Assembly Plan
#  a. Motor A and B will be seperated, both facing upwards. They will also be stavilized, using duct tap and cardboard. \n
#  b. Two platforms, one per motor, will be constructed from cardboard and fit onto motor, so that it spins with motor. \n
#  c. IMU will be placed on one motor, with wires long enough to be twisted and still connected. \n
#  d. Laser pointer placed on other motor. \n
#  e. Keypad will rest to the side. \n
#  f. If time permits, ultrasonic sensor will be positioned facing frontwards.
#
#
#  @section sec_safety Safety Plan
#  When testing, sunglasses will be worn, in addition to the door being closed in my room. \n
#  This will prevent others (and myself) from risking eye damage.
#
#
#  @section sec_timeline Timeline
# Today, I will order the laser pointer. \n
# Next week (week of 5/24), keypad module will be completed. \n
# Within a week after, the project will be constructed using tape, cardboard, and love.
#
#
#
#
#
#  @copyright License Info
#
#  @date May 22, 2020
#
