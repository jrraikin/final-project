'''
@file mode_dist.py

@brief      How Distance Mode Works:
@details    This page demonstrates how distance mode works.

@page       page_dist Distance

@section    sec_vid Video of Operation
@details    https://drive.google.com/file/d/1VmoB4lyndOM-PrR3X8kQ5NBB9yQa02Ow/view?usp=sharing

@section    sec_soft_arch Software Architecture
@image      html mode_dist.jpg width=50%
'''
