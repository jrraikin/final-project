##
# @page page_mode  Modes List
#                   This project has three modes of functionality.
#                   The following classes are present:
#                   - @subpage page_dc "Duty Cycle"
#                   - @subpage page_dist "Distance"
#                   - @subpage page_degs "Degrees"
#
#                   Link to all photos and videos: https://drive.google.com/drive/folders/1-Hsd8qVF22L2AKozqWgCzLbbWv0YWYnO?usp=sharing
