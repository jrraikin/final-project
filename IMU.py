'''
@file IMU.py

@brief      IMU module.
@details    This allows for using an IMU to collect euler angles and angular velocity,
            with the addition of gravity and temperature as a bonus. \n
            Link to code: https://bitbucket.org/jrraikin/final-project/src/master/IMU.py
'''

from pyb import I2C
import utime

reg_mode = const(0x3D)
reg_calib = const(0x35)

reg_PITCH_LSB = const(0x1E)
reg_ROLL_LSB = const(0x1C)
reg_YAW_LSB = const(0x1A)

reg_avel_x_LSB = const(0x14)
reg_avel_y_LSB = const(0x16)
reg_avel_z_LSB = const(0x18)

reg_grav_x_LSB = const(0x2E)
reg_grav_y_LSB = const(0x30)
reg_grav_z_LSB = const(0x32)

reg_temp = const(0x34)

def main():
    '''Creates object 'imu',  which initiazes, and prints results for gravity,
    euler angles, angular velocity, and temperature. The while loop is done 200 times,
    and after it disables. \n
    SCL --> D15 \n
    SDA --> D14
    '''
    imu = IMU(1)

    i = 0
    while(i < 201):

        print("\nGravity: ")
        print("\t", imu.gravity())

        print("\nEuler Angles: ")
        print("\t", imu.euler_angles())

        print("\nAngle Velocities: ")
        print("\t", imu.ang_veloc())

        print('\nTemperature: ')
        print('\t', imu.temp())

        utime.sleep_ms(1000)
        i += 1
    imu.disable_IMU()


class IMU:
    '''Sets up IMU.
    '''
    def __init__(self, bus):
        '''Initiliazes IMU by:
        - establishing I2C with MASTER connection
        - checking to see that the right address is scanned for "40"
        - if the right address is scanned for, enabling IMU with the correct mode (NDOF)
        - else: disable IMU
        @param bus establishes bus for the connection
        '''
        self._bus = bus
        self._i2c = I2C(self._bus, I2C.MASTER)
        self._baud = 400000
        #add_respond =  self._i2c.scan()
        add_respond = [40]
        ## Address of IMU's I2C connection. looking for 40 with bus 1.
        self.address = 0
        #check to see add is 40 (IMU)
        for add in add_respond:
            if add == 40:
                self.address = add
                break
        ##Calibration of SYS (0-3)
        self.SYS_calib = 0
        ##Calibration of GYR (0-3)
        self.GYR_calib = 0
        ##Calibration of ACC (0-3)
        self.ACC_calib = 0
        ##Calibration of MAG (0-3)
        self.MAG_calib = 0
        ##All calibration stats in a tuple.
        self.calib_stat = (0, 0, 0, 0)
        #if add == 40, enable IMU
        if(self.address is 40):
        #    if(self._i2c.is_ready(self.address)):
            self.mode_IMU(0b1100) #Fusion Mode, NDOF
            self.enable_IMU()
        else:
            self.disable_IMU()


    def enable_IMU(self):
        '''Enables the IMU. Checks that every calibration value is 3 before passing.
        Calibration values: SYS, GYR, ACC, MAG
        '''
        print('enabling IMU...')
        self.calibration_stat()
        i = 0
        #while (self.calib_stat != (3, 3, 3, 3) and i < 2000):
        while (self.calib_stat[1] != 3 and i < 2000):
            print('\nCalibration:', self.calib_stat, '\ti:', i)
            print("\n\tEuler Angles:", self.euler_angles())
            print("\n\tAngle Velocities:", self.ang_veloc())
            self.calibration_stat()
            i += 1
            utime.sleep_ms(1000)
        print('IMU Calibrated')


    def disable_IMU(self):
        '''Disables IMU
        '''
        print('\ndisabling IMU...')
        self._i2c.deinit()


    def mode_IMU(self, mode = None):
        '''Sets mode of IMU. NDOF is the only mode accepted to get values desires.
        @param mode   mode of IMU can be inputted. If not, False is returned.
        @return True if mode matches NDOF, False otherwise
        '''
        self._buff = bytearray(1)
        self._buff[0] = mode
        self._i2c.mem_write(self._buff, 40, reg_mode, timeout=5000, addr_size = 8)
        if (mode == 0b1100):
            return True
        else:
            return False


    def calibration_stat(self):
        '''Checks calibration status for SYS, GYR, ACC, MAG and shifts bit values as necessary.
        @return Tuple of all values (0-3) returned.
        '''
        #Calibration Status
        calibs = self._i2c.mem_read(1, self.address, reg_calib, timeout = 5000, addr_size = 8)
        ##Calibration of SYS (0-3)
        self.SYS_calib = (calibs[0] & 0b11000000) >> 6
        ##Calibration of GYR (0-3)
        self.GYR_calib = (calibs[0] & 0b00110000) >> 4
        ##Calibration of ACC (0-3)
        self.ACC_calib = (calibs[0] & 0b00001100) >> 2
        ##Calibration of MAG (0-3)
        self.MAG_calib = (calibs[0] & 0b00000011) >> 0
        ##All calibration stats in a tuple.
        self.calib_stat = (self.SYS_calib, self.GYR_calib, self.ACC_calib, self.MAG_calib)
        return self.calib_stat

    def euler_angles(self):
        '''Finds euler angles for pitch, roll, and heading/yaw. Divides aquired value by 16 to get correct result.
        @return Tuple of all values (degrees).
        '''
        #PITCH
        euler_ang_pitch = (self._i2c.mem_read(2, self.address, reg_PITCH_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(euler_ang_pitch)
        ##Euler Angle for pitch
        self.euler_ang_pitch = val / 16

        #ROLL
        euler_ang_roll = (self._i2c.mem_read(2, self.address, reg_ROLL_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(euler_ang_roll)
        ##Euler Angle for roll
        self.euler_ang_roll = val / 16

        #HEADING
        euler_ang_head = (self._i2c.mem_read(2, self.address, reg_YAW_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(euler_ang_head)
        ##Euler Angle for heading/yaw
        self.euler_ang_head = val / 16

        ##Tuple for Euler Angles
        self.angles = (self.euler_ang_head, self.euler_ang_roll, self.euler_ang_pitch)
        return(self.angles)

    def ang_veloc(self):
        '''Find angular velocities (x, y, z) using gyroscope.
        @return Tuple of (x,y,z) returned.
        '''
        #X
        ang_v_x = (self._i2c.mem_read(2, self.address, reg_avel_x_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(ang_v_x)
        ##Angular velocity, x-dir
        self.ang_v_x = val / 16

        #Y
        ang_v_y = (self._i2c.mem_read(2, self.address, reg_avel_y_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(ang_v_y)
        ##Angular velocity, y-dir
        self.ang_v_y = val / 16

        #Z
        ang_v_z = (self._i2c.mem_read(2, self.address, reg_avel_z_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(ang_v_z)
        ##Angular velocity, z-dir
        self.ang_v_z = val / 16

        ##Tuple for Angular velocities.
        self.velocs = (self.ang_v_x, self.ang_v_y, self.ang_v_z)
        return(self.velocs)


    def gravity(self):
        '''Find gravity in x,y, and z direction in m/s^2.
        @return Tuple of (x, y, z) returned.
        '''
        #X
        g_x = (self._i2c.mem_read(2, self.address, reg_grav_x_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(g_x)
        ##Gravity in x-dir
        self.g_x = val / 100

        #Y
        g_y = (self._i2c.mem_read(2, self.address, reg_grav_y_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(g_y)
        ##Gravity in y-dir
        self.g_y = val / 100

        #Z
        g_z = (self._i2c.mem_read(2, self.address, reg_grav_z_LSB, timeout = 5000, addr_size = 8))
        val = byte_to_bit(g_z)
        ##Gravity in z-dir
        self.g_z = val / 100

        ##Tuple of gravity values.
        self.grav = (self.g_x, self.g_y, self.g_z)
        return(self.grav)


    def temp(self, unit = 'F'):
        '''Temperature determined in degrees.
        @param unit Defualt fahrenheit, unless 'C' is inputted.
        @return Temperature output in desired unit.
        '''
        t = (self._i2c.mem_read(1, self.address, reg_temp, timeout = 5000, addr_size = 8))
        val = t[0]
        if (unit == 'F'):
            self._t = (val * 9 / 5) + 32
        else:
            self._t = val
        return self._t
        # 0(°C × 9/5) + 32 = 32°F


def byte_to_bit(byte):
    '''Takes in byte data from register and shifts it to collect it accurately.
    @param byte Data collectedc from registers LSB and MSB.
    @return value of registers data returned.
    '''
    val = (byte[0] | byte[1]<<8)
    if (val > 32767):
        val -= 65536
    elif (val < -32767):
        val += 65536
    return val


if __name__ == '__main__':
    main()
