'''
@file Location.py

@brief      Encoder module.
@details    This allows one to get updates on how much the motor has spun, in either direction, and accounts for overflow. \n
            Link to code: https://bitbucket.org/jrraikin/final-project/src/master/Location.py
'''
import pyb
import utime

def main():
    '''Establishes timer, encoders A and B pins, and sets up Encoder module.'''
    ''''Motor 1'''
    timer = 4 #page 93 datasheet
    pin_encA = pyb.Pin.cpu.B6
    pin_encB = pyb.Pin.cpu.B7

    '''Motor 2
    timer = 8
    pin_encA = pyb.Pin.cpu.C6
    pin_encB = pyb.Pin.cpu.C7
    '''

    enc = Encoder(pin_encA, pin_encB, timer)

    while(1):
        enc.update()
        print("position: {0}".format(enc.get_position()))
        print("delta: {0}".format(enc.get_delta()))
        utime.sleep_ms(500)
        #enc.set_position(30)

    '''
    encA = pyb.Pin(pin_encA, pyb.Pin.IN)
    encB = pyb.Pin(pin_encB, pyb.Pin.IN)
    prescaler = 0 # Timers 2-7 and 12-14 have a clock source of 84 MHz (pyb.freq()[2] * 2)
    period = 0xffff # [0-0x3fffffff] for timers 2 & 5
    timer = pyb.Timer(timer, prescaler = prescaler, period = period)
    ch2 = timer.channel(2, timer.ENC_AB, pin = encB)
    ch1 = timer.channel(1, timer.ENC_AB, pin = encA)
    lastRead = timer.counter()
    while(1):
        print(timer.counter())
        utime.sleep_ms(500)
    '''



class Encoder:
    '''Sets up a timer in encoder counting mode.
    '''
    def __init__(self, enc_A, enc_B, timer):
        '''Does the setup, given appropriate parameters such as which pins and timer to use.
        Also configures channels and establishes the first lastRead value.
        @param enc_A    A pyb.Pin that establishes encoder A's pin
        @param enc_B    A pyb.Pin that establishes encoder B's pin
        @param timer    Input for appropriate timer, as motor A pins and motor B pins use different timers
        '''
        ## A pyb.Pin that establishes encoder A's pin
        self.enc_A = enc_A
        ## A pyb.Pin that establishes encoder B's pin
        self.enc_B = enc_B
        ## Input for appropriate timer, as motor A pins and motor B pins use different timers
        self.timer = timer

        self._prescaler = 0 # Timers 2-7 and 12-14 have a clock source of 84 MHz (pyb.freq()[2] * 2)
        self._period = 0xffff

        encA = pyb.Pin(self.enc_A, pyb.Pin.IN)
        encB = pyb.Pin(self.enc_B, pyb.Pin.IN)
        self.timer = pyb.Timer(self.timer, prescaler = self._prescaler, period = self._period)
        ch2 = self.timer.channel(2, self.timer.ENC_AB, pin = encB)
        ch1 = self.timer.channel(1, self.timer.ENC_AB, pin = encA)
        self._lastRead = self.timer.counter() #tells timer count (tick value until overflow)
        self._runningTot = 0

    def update(self):
        '''When called regularly, updates the recorded position of the encoder.
        Find the delta value, and adds it to running total.
            If delta value happens to go over or below the period / 2,
            (2^16 -1) is deducted or added from/to it.
        '''
        self._new_val = self.timer.counter()
        self._delta = self._new_val - self._lastRead
        if (self._delta >= (self._period / 2)):
            self._delta -= (2**16) #overflow = 2**16 -1
        elif(self._delta < -(self._period / 2)):
            self._delta += (2**16)
        self._lastRead = self._new_val
        self._runningTot += self._delta


    def get_position(self):
        '''Returns the most recently updated position of the encoder (self.runningTot).
        '''
        return int(self._runningTot)

    def set_position(self, value):
        '''Resets the position to to a specified value.
        @param value    Input for desired location of motor.
        '''
        self._lastRead = self.timer.counter()
        self._runningTot = value

    def get_delta(self):
        '''Returns the difference in recorded position between
        the two most recent calls to update().
        '''
        return self._delta


if __name__ == '__main__':
    main()
