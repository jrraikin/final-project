'''
@file Controller.py

@brief      Controller module.
@details    This utilizes a proportional controller in order to operate the motor, specifically using a step response. \n
            Link to code: https://bitbucket.org/jrraikin/final-project/src/master/Controller.py

'''
import pyb
import utime

from Motor import *
from Location import *

def main():
    '''Input for setpoint and Kp value, which samples 200 time and position values from the step response proportional controller.
    '''
    try:
        setpoint = int(input('set desired setpoint location of motor: '))
    except:
        raise ValueError('setpoint must be integer value')

    try:
        Kp = float(input('set Kp value: '))
    except:
        raise ValueError('Kp must be integer value')

    '''Motor 1'''
    #Encoder
    timerE = 4 #page 93 datasheet
    pin_encA = pyb.Pin.cpu.B6
    pin_encB = pyb.Pin.cpu.B7
    #Motor
    timerM = 3
    pin_EN = pyb.Pin.cpu.A10
    pin_IN1 = pyb.Pin.cpu.B4
    pin_IN2 = pyb.Pin.cpu.B5

    '''Motor 2
        #Encoder
        timerE = 8
        pin_encA = pyb.Pin.cpu.C6
        pin_encB = pyb.Pin.cpu.C7
        #Motor (for use of IN1B & IN2B)
        timerM = 5
        pin_EN = pyb.Pin.cpu.C1
        pin_IN1 = pyb.Pin.cpu.A0
        pin_IN2 = pyb.Pin.cpu.A1
    '''
    ##Initialize Encoder
    enc = Encoder(pin_encA, pin_encB, timerE)

    ##Initialize motor
    moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, timerM)
    #Enable motor
    moe.enable()

    ##Initialize Controller
    con = Controller(setpoint, Kp, enc, moe)

    time = []
    mot_pos = []

    i = 0
    t = utime.ticks_ms()
    while(i < 201):
        con.update()
        time.append(utime.ticks_ms() % t)
        enc.update()
        mot_pos.append(enc.get_position())
        utime.sleep_ms(10)
        i += 1
    moe.disable()
    print_tests(time, mot_pos)


class Controller:
    '''Sets up controller.
    '''
    def __init__(self, setpoint, Kp, encoder_class, motor_class):
        '''
        @param setpoint    The setpoint taken from main is used to know what value to go to for the step response
        @param Kp          This value (should be within range of 0 and 1 for bets results), sets the proportional scale for the response
        @param encoder_class    Encoder class object taken from Lab 2. Used to find position.
        @param motor_class      Motor class object taken from Lab 1. Used to drive motor.
        '''
        ## The setpoint taken from main is used to know what value to go to for the step response
        self.setpoint = setpoint

        ## This value (should be within range of 0 and 1 for bets results), sets the proportional scale for the response
        self.Kp = Kp

        ## Encoder class object taken from Lab 2. Used to find position.
        self.encoder_class = encoder_class

        ## Motor class object taken from Lab 1. Used to drive motor.
        self.motor_class = motor_class


    def update(self):
        '''When called regularly, updates the error value by finding the new position and subtracting it from the setpoint.
        Uses the error value obtained to determine the Actuation signal, which gets inputted into the motor driver.
        '''
        self._error = self.setpoint - self.encoder_class.get_position()

        #Actuation Signal
        self._act_sig = self.Kp * self._error

        #set motor
        self.motor_class.set_duty(self._act_sig * -1)

def print_tests(time, mot_pos):
    '''Formats gathered inputs from while loop in order to make a csv file for the graphs to be made.
    If somehow the lsits do not have the same length, the length of the shorter list is used to not have an index error.
    @param time    list of time (200 samples)
    @param mot_pos motor position list (200 samples)
    '''
    print("time (ms)\tposition")
    if len(time) < len(mot_pos):
        length = len(time)
    else:
        length = len(mot_pos)
    for i in range(length):
        print("{0},\t{1}".format(time[i], mot_pos[i]))


if __name__ == '__main__':
    main()
